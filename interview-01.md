# Frontend Interview 01
## First Round
Q1: Please describe your past work experience.

A1: A1: I self-taught front-end development in 2021 and joined a startup blockchain company where I worked as a blockchain developer. In the company, I was involved in the development of various blockchain-related projects, including swap, minertool, a no-code platform, blockchain wallets, and more.

Q2: What programming languages and frameworks do you commonly use?

A2: The programming languages I commonly use include JavaScript, TypeScript, Swift, and Dart. As for frameworks, I often work with Angular, React, Svelte, Vue, Nuxt, and Next. On the server side, I frequently use Node.js and NestJS.

Q3: Have you faced any difficulties in your job, and how did you go about solving them?

A3: "I have encountered challenges in my work, and I faced bugs in my code. When I encounter such issues, I try to troubleshoot and debug them independently. If I'm unable to resolve the problem on my own, I will seek assistance from colleagues. Additionally, I often turn to online forums like StackOverflow and Quora for solutions and insights.

## Second Round

Q1: Talk about the differences between cookies, sessions, and local storage.

A1: All of them could storage data in client-side. Cookies are used for maintaining user's specific information and have expiration dates. Cookies have a size limit of around 4KB per domain. Sessions is initiated when user get access to a website and destoried when closing the browser. Session's limit is usuallt higher, commonly be about 5-10MB per domain. LocalStorage is commonly used for caching data or maintaining the state of the web application. LocalStorage could store more data and without expiration date, the limit is usually up to 5-10MB or more whitch depending on the browser.

Q2: Have you heard about SEO? How to improve a website's SEO as a frontend developer?

A2: SEO optimization involves enhancing the ranking on search engine result pages. As a web developer, I optimized images and scripts for faster load times and I also ensure that we build pages with meta descriptions and title tags. Further more, I usually use SSR frameworks like nuxt and next. SSR helps reduce the appearance of skeleton screens on index page and ensures that search engine crawlers could easily access the website.

Q3: What hooks do you usually use? Explain the purposes of them.

A3: useState, useEffect, useMemo, useCallback, useRef, useContext, useReducer.(具体是共享画面写了几个例子, 只写了前面六个)

Q4: 蚂蚁爬格子，m*n， 只能向上或向右，从左下到右上，求路径数，如果中间有坏点（不能到达的点），路径又是多少。